local Aseprite = {}
local Aseprite_mt = {}

function Aseprite.new(file)

	local buff = love.filesystem.read(file)
	local tbl = { header = {}, frames = {} }
	tbl.header.filesz, tbl.header.magicn, tbl.header.frames, tbl.header.width, tbl.header.height, 
	tbl.header.depth, tbl.header.flags, tbl.header.speed, tbl.header.set1, tbl.header.set2, 
	tbl.header.transparentcol, tbl.header.ignorepls, tbl.header.numcols, tbl.header.pixel_w,
	tbl.header.pixel_h, tbl.header.x, tbl.header.y, tbl.header.grid_w, tbl.header.grid_h, tbl.header.zero,

	tbl.frames.bytes, tbl.frames.magicn, tbl.frames.chunks, tbl.frames.duration, tbl.frames.zero, tbl.frames.numchunks
	 = love.data.unpack('<IHHHHHIHIIBc3HBBhhHHc84IHHHc2I', buff)

	return setmetatable(tbl, Aseprite_mt)
end

return Aseprite