
local Aseprite = require "lib.Aseprite"

local ase = Aseprite.new("test.ase")

function love.draw()
	love.graphics.clear(0.1, 0.1, 0.1)
	love.graphics.setColor(1, 1, 1)
	love.graphics.print(ase.header.width .. "x" .. ase.header.height, 32, 32)
	love.graphics.print(ase.header.grid_w .. "x" .. ase.header.grid_h, 23, 48)
end