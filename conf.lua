function love.conf(t)
    t.window.width = 320*4
    t.window.height = 180*4
    t.window.vsync = false
    t.modules.physics = false
    t.window.fullscreen = false
end
